//
// Created by Ostrichbeta Yick-Ming Chan on 3/5/22.
//

#ifndef DSTASKPICKER_EXAMPLETASK_H
#define DSTASKPICKER_EXAMPLETASK_H
#include "TaskTemplate.h"

void test01Entry();
TaskTemplate task01("Example Test", "An example test class for test use.", test01Entry);

#endif //DSTASKPICKER_EXAMPLETASK_H
