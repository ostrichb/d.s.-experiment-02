#ifndef DSA_LIBDSA_H
#define DSA_LIBDSA_H
#include <string>

using std::string;
enum gender{MALE, FEMALE};
class Cstu {
private:
    int id{};
    string name;
    float age{};
    double score{};
    gender sex;
public:
    Cstu(int, const string&, float, double, gender);
    friend std::ostream & operator<< (std::ostream & os, const Cstu & stu);
};

#endif //DSA_LIBDSA_H
