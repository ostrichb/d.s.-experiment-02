//
// Created by Ostrichbeta Yick-Ming Chan on 3/9/22.
//

#ifndef WHUDATASTRUCTUREEXP02_TASK05_H
#define WHUDATASTRUCTUREEXP02_TASK05_H
#include "TaskTemplate.h"

void task05Entry();
const TaskTemplate task05("Constants and Combined type", "Instantiate an integer array and a Cstu array (both arrays have five elements), give values and print them.", task05Entry);

#endif //WHUDATASTRUCTUREEXP02_TASK05_H
