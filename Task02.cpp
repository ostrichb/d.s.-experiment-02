//
// Created by Ostrichbeta Yick-Ming Chan on 3/8/22.
// Task 02: Linear List and Vector
// Description: Create a vector and simple linear list, and tell their difference.

#include "Task02.h"
#include <vector>
#include <iostream>

using std::vector;

void task02Entry(){
    // Declare a vector
    vector<int> intList;
    for (int i = 1; i <= 20; ++i) {
        intList.push_back(i * i);
    }
    for (int i = 1; i <= 5; ++i) {
        intList.insert(intList.begin() + 9 + i, i * i * i);
    }
    std::cout << "This is the element of a vector: ";
    for ( const int & item : intList) {
        std::cout << item << " ";
    }
    std::cout << std::endl;

    // A vector has the feature to dynamically add and remove elements easily,
    // without spend a host of time allocating or releasing memory by ourselves.

}