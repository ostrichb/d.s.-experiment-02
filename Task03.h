//
// Created by Ostrichbeta Yick-Ming Chan on 3/8/22.
//

#ifndef WHUDATASTRUCTUREEXP02_TASK03_H
#define WHUDATASTRUCTUREEXP02_TASK03_H
#include "TaskTemplate.h"

void task03Entry();
const TaskTemplate task03("Instantiation", "Instantiate the Cstu Class and listed in a vector list.", task03Entry);


#endif //WHUDATASTRUCTUREEXP02_TASK03_H
