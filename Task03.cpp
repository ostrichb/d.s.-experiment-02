//
// Created by Ostrichbeta Yick-Ming Chan on 3/8/22.
// Task 03: Instantiation
// Description: Instantiate the Cstu Class and listed in a vector list.

#include "Task03.h"
#include "libdsa.h"
#include <vector>
#include <iostream>
#include <random>
#include <array>

void task03Entry(){
    using std::vector;
    vector<Cstu> studentList;
    std::random_device rd;
    std::mt19937 gen(rd());
    std::uniform_real_distribution<> dis(80, 100);
    std::array<string, 10> nameList = {"Ostrichbeta Yick-Ming Chan", "Fanny Li", "Malo Deng",
                                       "WeChat Long", "From Li", "Tina Tang", "Cheung Tsui", "Lai-hin Wai",
                                       "Cloude Wun", "Yu Zheung"};
    for (int i = 0; i < 10 ; i++) {
        studentList.push_back(Cstu(9000 + i, nameList[i], 19, dis(gen), static_cast<gender>(i % 2)));
    }
    for (const Cstu & item : studentList) {
        std::cout << item << std::endl;
    }
}