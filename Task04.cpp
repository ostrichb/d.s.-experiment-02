//
// Created by Ostrichbeta Yick-Ming Chan on 3/9/22.
// Task 04: Queues and Stacks
// Description: Reverse a queue and put the result into a stack instance.
#include <queue>
#include <stack>
#include <iostream>
#include <random>

namespace randspace {
    // Declare a random number generator
    std::random_device rd;
    std::mt19937_64 gen(rd());
    std::uniform_int_distribution<int> dis(0, 100);
}

void task04Entry(){
    std::queue<int> intQueue;
    for (int i = 0; i < 20; ++i) {
        intQueue.push(randspace::dis(randspace::gen));
    }
    // A queue cannot be accessed freely without popping items.
    // Therefore, we just dequeue it in order to get the values.
    std::stack<int> intStack;
    while (!intQueue.empty()) {
        std::cout << "Popping element " << intQueue.front() << " to intQueue... ";
        intStack.push(intQueue.front());
        intQueue.pop();
        std::cout << "OK." << std::endl;
    }
    // Now accessing the stack
    std::cout << "Reversed Elements in the stack are: ";
    while(!intStack.empty()){
        std::cout << intStack.top() << (intStack.size() == 1 ? "" : ", ");
        intStack.pop();
    }
    std::cout << std::endl;
}
