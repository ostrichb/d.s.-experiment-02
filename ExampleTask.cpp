//
// Created by Ostrichbeta Yick-Ming Chan on 3/4/22.
// This is an example task, all the tasks should follow this standard.
// Note that the compatibility is C++23, so there may be some unsupported codes appear in Visual Studio.
// Task 00: Example Task


// A sample function that sums up every number between begin and end.
int sumOf(int start, int end) {
    int result {0};
    for (int i = start; i <= end; ++i) {
        result += i;
    }
    return result;
}

void test01Entry(){
    sumOf(1, 100);
}