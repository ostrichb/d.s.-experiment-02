#include <iostream>
#include <vector>
#include <ctime>
#include "Task02.h"
#include "Task03.h"
#include "Task04.h"
#include "Task05.h"

/*
 * All the homeworks will have this file as the main entry. Although there will always be a main function
 * in each cpp class, use this picker is more convenient as standard unittest tasks will be executed here,
 *
 */

int main() {
    using namespace std;
    vector<TaskTemplate> tasks;

    // Define a time structure.
    timespec ts{};

    // Add the tasks here
    tasks.push_back(task02);
    tasks.push_back(task03);
    tasks.push_back(task04);
    tasks.push_back(task05);

    // Print out the tasks
    cout << "====  Boot Picker ====" << endl;
    string pkg_name = "Data Structure Experiment 02";
    string complete_date = "Mar 10 2022";
    cout << "Package Name: " << pkg_name << endl;
    cout << "Complete Date: " << complete_date << endl << endl;

    if (tasks.empty()) {
        cout << "There is no tasks listed above, please add one to run." << endl;
    } else {
        int count = 0;
        for_each(tasks.begin(), tasks.end(), [&](TaskTemplate &item){
           cout << (++count) << ". " << item.getTitle() << endl;
           cout << "    " << item.getDescription() << "\n" << endl;
        });
        int picker;
        do {
            cout << "Enter your boot entry: ";
            picker = 0;
            if (cin.fail()) {
                cin.clear();
                cin.ignore(numeric_limits<streamsize>::max(), '\n');
            }
            cin >> picker;
        }
        while (!(picker >= 1 && picker <= tasks.size()));
        cout << "You chose program " << picker << " to run.\n" << endl;
        timespec_get(&ts, TIME_UTC);
        long double initial_time = ts.tv_sec + (long double) ts.tv_nsec / 1000000000;
        tasks[picker - 1].entry();
        timespec_get(&ts, TIME_UTC);
        long double diff = ts.tv_sec + (long double) ts.tv_nsec / 1000000000 - initial_time;
        cout.setf(ios::fixed);
        cout << "Run complete. The running time is " << diff << " second(s)." << endl;
        cout.unsetf(ios::fixed);
    }
    return 0;
}
