//
// Created by Ostrichbeta Yick-Ming Chan on 3/9/22.
//

#ifndef WHUDATASTRUCTUREEXP02_TASK04_H
#define WHUDATASTRUCTUREEXP02_TASK04_H
#include "TaskTemplate.h"

void task04Entry();
const TaskTemplate task04("Queues and Stacks", "Reverse a queue and put the result into a stack instance.", task04Entry);

#endif //WHUDATASTRUCTUREEXP02_TASK04_H
