//
// Created by Ostrichbeta Yick-Ming Chan on 3/9/22.
// Task 05: Constants and Combined type
// Description: Instantiate an integer array and a Cstu array (both arrays have five elements), give values and print them.

#include "libdsa.h"
#include <iostream>
#include <array>
#include <random>

namespace randomspace {
    std::random_device rd;
    std::mt19937_64 gen(rd());
    std::uniform_int_distribution dis(-99, 99);
}

void task05Entry(){
    using std::array;
    using namespace randomspace;
    array<int, 5> intArray{};
    std::cout << "Elements in intArray are: ";
    for (int & item : intArray) {
        item = dis(gen);
        std::cout << item << " ";
    }
    std::cout << std::endl;

    array<Cstu, 5> stuArray{
        Cstu(9001, "Ostrichbeta Chan", 19, (double) intArray[3], MALE),
        Cstu(9002, "Yu-Hin Wai", 19.6, (double) intArray[4], FEMALE),
        Cstu(9003, "Fanny Lai", 19, (double) intArray[0], FEMALE),
        Cstu(9004, "Ostrichbeta Chan", 19, (double) intArray[1], MALE),
        Cstu(9005, "Ostrichbeta Chan", 19, (double) intArray[2], MALE)
    };

    for (const Cstu & item : stuArray) {
        std::cout << item << std::endl;
    }


}

