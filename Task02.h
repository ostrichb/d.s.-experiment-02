//
// Created by Ostrichbeta Yick-Ming Chan on 3/8/22.
//

#ifndef WHUDATASTRUCTUREEXP02_TASK02_H
#define WHUDATASTRUCTUREEXP02_TASK02_H
#include "TaskTemplate.h"

void task02Entry();
const TaskTemplate task02("Linear List and Vector", "Create a vector and simple linear list, and tell their difference.", task02Entry);


#endif //WHUDATASTRUCTUREEXP02_TASK02_H
